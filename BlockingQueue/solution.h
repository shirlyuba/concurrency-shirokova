#include <iostream>
#include <condition_variable>
#include <queue>
#include <mutex>
#include <stdexcept>


template <class T, class Container = std::deque<T>>
class BlockingQueue {

 private:

  Container queue;

  std::mutex mutex;

  std::condition_variable cv_get;

  std::condition_variable cv_put;

  std::size_t queue_capacity;

  bool off = false;

 public:

  explicit BlockingQueue(const size_t& capacity) : queue_capacity(capacity) {};

  // You need to implement Put method which will block and wait while capacity
  // of your container exceeded. It should work with noncopyable objects and
  // use move semantics for placing elements in the container. If the queue is 
  // shutdown you need to throw an exception inherited from std::exceptiono.
  void Put(T&& el) {
    std::unique_lock<std::mutex> lock(mutex);
    while (!off && queue.size() == queue_capacity) {
      cv_put.wait(lock);
    }
    if (!off) {
      queue.push_back(std::move(el));
      cv_get.notify_one();
    } else {
      throw std::runtime_error("Queue is off!");
    }
  }

  // Get should block and wait while the container is empty, if the queue
  // shutdown you will need to return false.
  bool Get(T& result) {
    std::unique_lock<std::mutex> lock(mutex);
    while (!off && queue.empty()) {
      cv_get.wait(lock);
    }
    if (!off) {
      result = std::move(queue.front());
      queue.pop_front();
      cv_put.notify_one();
      return true;
    }
    return false;
  }

  // Shutdown should turn off the queue and notify every thread which is waiting
  // for something to stop wait.
  void Shutdown() {
    std::unique_lock<std::mutex> lock(mutex);
    off = true;
    cv_get.notify_all();
    cv_put.notify_all();
  }
};
