#include <iostream>
#include <thread>
#include <atomic>
#include <array>
#include <vector>
#include <cmath>


class PetersonMutex {

 private:

  std::array<std::atomic<bool>, 2> want;

  std::atomic<int> victim;

 public:

  PetersonMutex() {
    want[0].store(false);
    want[1].store(false);
    victim.store(0);
  }


  void lock(int t) {
    want[t].store(true);
    victim.store(t);
    while (want[1 - t].load() && victim.load() == t) {
     std::this_thread::yield();
    }
  }


  void unlock(int t) {
    want[t].store(false);
  }
};


class TreeMutex {

 private:

  size_t count_of_nodes;

  std::vector<PetersonMutex> nodes;

 public:

  TreeMutex(size_t n_threads) {
    if (n_threads == 1) {
      count_of_nodes = 1;
    } else {
      count_of_nodes = (size_t)(1 << (int)((log(n_threads - 1) / log(2)) + 1)) - 1;
    }
    nodes = std::vector<PetersonMutex> (count_of_nodes);
 }


  void lock(size_t current_thread) {
    size_t current_node = count_of_nodes + current_thread;
    while (current_node != 0) {
      int prev_node = current_node;
      current_node = (current_node - 1) / 2;
      nodes[current_node].lock(prev_node % 2);
    }
  }


  void unlock(size_t current_thread) {
    size_t current_node = 0;
    size_t next_node;
    size_t current_index_of_thread = current_thread + 1;
    size_t mode = (count_of_nodes + 1) / 2;
    while (current_node != count_of_nodes + current_thread) {
      if (current_index_of_thread > mode) {
        next_node = current_node * 2 + 2;
        current_index_of_thread -= mode;
      } else {
        next_node = current_node * 2 + 1;
      }
      mode /= 2;
      nodes[current_node].unlock(next_node % 2);
      current_node = next_node;
    }
  }
};
