#pragma once

#include <functional>
#include <vector>
#include <forward_list>
#include <deque>
#include <mutex>
#include <atomic>


class RWLock{

 private:

 int readers;
 int writer_count;
 bool writing;
 std::condition_variable cv;
 std::mutex mutex;

 public:

 void LockRead() {
   std::unique_lock<std::mutex> lock(mutex);
   while (writer_count > 0) {
     cv.wait(lock);
   }
   ++readers;
 }

 void UnlockRead() {
   std::unique_lock<std::mutex> lock(mutex);
   --readers;
   if (readers == 0) {
     cv.notify_all();
   }
 }

 void LockWrite() {
   std::unique_lock<std::mutex> lock(mutex);
   ++writer_count;
   while (writing || (readers > 0)) {
     cv.wait(lock);
   }
   writing = true;
 }

 void UnlockWrite() {
   std::unique_lock<std::mutex> lock(mutex);
   --writer_count;
   writing = false;
   cv.notify_all();
 }

};



template <typename T, class Hash = std::hash<T>>
class StripedHashSet {
 public:
  explicit StripedHashSet(const size_t concurrency_level_,
                          const size_t growth_factor_ = 3,
                          const double load_factor_ = 0.75) {
    concurrency_level = concurrency_level_;
    growth_factor = growth_factor_;
    load_factor = load_factor_;
    current_size.store(0);
    locks.resize(concurrency_level);
    hash_table.resize(concurrency_level * growth_factor);
  }

  bool Insert(const T& element) {
    size_t element_hash_value = Hash()(element);
    locks[GetStripeIndex(element_hash_value)].LockWrite();
    if (find(hash_table[GetBucketIndex(element_hash_value)].begin(),
             hash_table[GetBucketIndex(element_hash_value)].end(),
             element) == hash_table[GetBucketIndex(element_hash_value)].end()) {
      hash_table[GetBucketIndex(element_hash_value)].push_front(element);
      current_size.fetch_add(1);
      if (current_size.load() / hash_table.size() > load_factor) {
        locks[GetStripeIndex(element_hash_value)].UnlockWrite();
        Expand();
        return true;
      }
      locks[GetStripeIndex(element_hash_value)].UnlockWrite();
      return true;
    }
    locks[GetStripeIndex(element_hash_value)].UnlockWrite();
    return false;
  }

  bool Remove(const T& element) {
    size_t element_hash_value = Hash()(element);
    locks[GetStripeIndex(element_hash_value)].LockWrite();
    if (find(hash_table[GetBucketIndex(element_hash_value)].begin(),
             hash_table[GetBucketIndex(element_hash_value)].end(),
             element) != hash_table[GetBucketIndex(element_hash_value)].end()) {
      hash_table[GetBucketIndex(element_hash_value)].remove(element);
      current_size.fetch_sub(1);
      locks[GetStripeIndex(element_hash_value)].UnlockWrite();
      return true;
    }
    locks[GetStripeIndex(element_hash_value)].UnlockWrite();
    return false;
  }

  bool Contains(const T& element) {
    size_t element_hash_value = Hash()(element);
    locks[GetStripeIndex(element_hash_value)].LockRead();
    if(find(hash_table[GetBucketIndex(element_hash_value)].begin(),
            hash_table[GetBucketIndex(element_hash_value)].end(),
            element) != hash_table[GetBucketIndex(element_hash_value)].end()) {
      locks[GetStripeIndex(element_hash_value)].UnlockRead();
      return true;
    }
    locks[GetStripeIndex(element_hash_value)].UnlockRead();
    return false;
  }

  size_t Size() {
    return current_size;
  }

  void Expand() {
    locks[0].LockWrite();
    if (current_size.load() / hash_table.size() > load_factor)  {
      for (size_t i = 1; i < locks.size(); ++i) {
        locks[i].LockWrite();
      }
      std::vector<std::forward_list<T>> new_hash_table(hash_table.size() * growth_factor);
      for (size_t i = 0; i < hash_table.size(); ++i) {
        for (auto it = hash_table[i].begin(); it != hash_table[i].end(); ++it) {
          size_t element_hash_value = Hash()(*it);
          new_hash_table[element_hash_value % new_hash_table.size()].push_front(*it);
        }
      }
      hash_table.swap(new_hash_table);
      for (size_t i = 1; i < locks.size(); ++i) {
        locks[i].UnlockWrite();
      }
    }
    locks[0].UnlockWrite();
  }

 private:
  size_t GetBucketIndex(const size_t element_hash_value) const {
    return element_hash_value % hash_table.size();
  }

   size_t GetStripeIndex(const size_t element_hash_value) const {
     return element_hash_value % concurrency_level;
   }

  std::atomic<size_t> current_size;
  size_t concurrency_level;
  size_t growth_factor;
  double load_factor;
  std::deque<RWLock> locks;
  std::vector<std::forward_list<T>> hash_table;
};

template <typename T> using ConcurrentSet = StripedHashSet<T>;