#pragma once

#include "arena_allocator.h"

#include <atomic>
#include <limits>

///////////////////////////////////////////////////////////////////////

template <typename T>
struct ElementTraits {
	static T Min() {
	  return std::numeric_limits<T>::min();
	}
	static T Max() {
	  return std::numeric_limits<T>::max();
	}
};

///////////////////////////////////////////////////////////////////////

class SpinLock {
 public:
  explicit SpinLock() : locked(false) {}

  void Lock() {
    while (locked.exchange(true)) {
      std::this_thread::yield();
    }
  }

  void Unlock() {
    locked.store(false);
  }

 // adapters for BasicLockable concept

  void lock() {
    Lock();
  }

  void unlock() {
    Unlock();
  }

 private:
  std::atomic<bool> locked;

};

///////////////////////////////////////////////////////////////////////

template <typename T>
class OptimisticLinkedSet {
 private:
  struct Node {
	  T element_;
	  std::atomic<Node*> next_;
	  SpinLock lock_{};
	  std::atomic<bool> marked_{false};
	  Node(const T& element, Node* next = nullptr)
		  : element_(element),
		    next_(next) {
	  }
  };

  struct Edge {
	  Node* pred_;
	  Node* curr_;
	  Edge(Node* pred, Node* curr)
		  : pred_(pred),
		    curr_(curr) {
	  }
  };

 public:
  explicit OptimisticLinkedSet(ArenaAllocator& allocator)
	  : allocator_(allocator) {
      CreateEmptyList();
  }

  bool Insert(const T& element) {
    while (true) {
      auto edge = Locate(element);
      std::unique_lock<SpinLock> lock_pred(edge.pred_->lock_);
      std::unique_lock<SpinLock> lock_curr(edge.curr_->lock_);
      if (Validate(edge)) {
        if (edge.curr_->element_ == element  || edge.curr_ == nullptr) {
          return false;
        } else {
          Node* added_node = allocator_.New<Node>(element);
          added_node->next_.store(edge.curr_);
          edge.pred_->next_.store(added_node);
          current_size.fetch_add(1);
          return true;
        }
      }
    }
  }

  bool Remove(const T& element) {
    while (true) {
      auto edge = Locate(element);
      std::unique_lock<SpinLock> lock_pred(edge.pred_->lock_);
      std::unique_lock<SpinLock> lock_curr(edge.curr_->lock_);
      if (Validate(edge)) {
        if (edge.curr_->element_ != element || edge.curr_ == nullptr) {
          return false;
        } else {
          edge.curr_->marked_.store(true);
          edge.pred_->next_.store(edge.curr_->next_);
          current_size.fetch_sub(1);
          return true;
        }
      }
    }
  }

  bool Contains(const T& element) const {
    Edge edge = Locate(element);
    if (edge.curr_ == nullptr) {
      return false;
    }
    return edge.curr_->element_ == element && !edge.curr_->marked_;
  }

  size_t Size() const {
    return current_size;
  }

 private:
  void CreateEmptyList() {
    head_ = allocator_.New<Node>(ElementTraits<T>::Min());
    head_->next_ = allocator_.New<Node>(ElementTraits<T>::Max());
  }

  Edge Locate(const T& element) const {
    Node* pred = head_;
    Node* curr = head_->next_;
    while (curr->element_ < element) {
      pred = curr;
      curr = curr->next_;
    }
    return Edge{pred, curr};
  }

  bool Validate(const Edge& edge) const {
    return !edge.pred_->marked_ && !edge.curr_->marked_ && edge.pred_->next_ == edge.curr_;
  }

 private:
  ArenaAllocator& allocator_;
  Node* head_{nullptr};
  std::atomic<size_t> current_size{0};
};

template <typename T> using ConcurrentSet = OptimisticLinkedSet<T>;

///////////////////////////////////////////////////////////////////////
