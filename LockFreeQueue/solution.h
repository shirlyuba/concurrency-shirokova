#pragma once

#include <thread>
#include <atomic>

///////////////////////////////////////////////////////////////////////

template <typename T, template <typename U> class Atomic = std::atomic>
class LockFreeQueue {
  struct Node {
	  T element_{};
	  Atomic<Node*> next_{nullptr};

	  explicit Node(T element, Node* next = nullptr)
		  : element_(std::move(element))
		  , next_(next) {
	  }

	  explicit Node() {
	  }
  };

 public:
  explicit LockFreeQueue() {
    Node* dummy = new Node{};
    head_ = dummy;
    top_of_deleted_ = dummy;
    tail_ = dummy;
  }

  ~LockFreeQueue() {
    Node* deleted_node = top_of_deleted_.load();
    while (deleted_node != nullptr) {
      Node* tmp = deleted_node;
      deleted_node = deleted_node->next_.load();
      delete tmp;
    }
  }

  void Enqueue(T element) {
    counter_.fetch_add(1);
    Node* new_tail = new Node(element);
    Node* curr_tail;
    while (true) {
      curr_tail = tail_.load();
      Node *tail_next = curr_tail->next_.load();
      if (tail_.load() == curr_tail) {
        if (!tail_next) {
          if (curr_tail->next_.compare_exchange_weak(tail_next, new_tail)) {
            break;
          }
        } else {
          tail_.compare_exchange_weak(curr_tail, tail_next);
        }
      }
    }
    tail_.compare_exchange_weak(curr_tail, new_tail);
    counter_.fetch_sub(1);
  }

  bool Dequeue(T& element) {
    counter_.fetch_add(1);
    Node* curr_head;
    Node* curr_tail;
    Node* head_next;
    while (true) {
      curr_head = head_.load();
      curr_tail = tail_.load();
      head_next = curr_head->next_.load();
      if (head_.load() == curr_head) {
        if (curr_head == curr_tail) {
          if (!head_next) {
            counter_.fetch_sub(1);
            return false;
          } else {
            tail_.compare_exchange_weak(curr_tail, head_next);
          }
        } else {
          if (head_.compare_exchange_weak(curr_head, head_next)) {
            element = head_next->element_;
            if (counter_.load() == 1) {
              Node* curr_deleted_node = top_of_deleted_.exchange(head_next);
              while (curr_deleted_node != top_of_deleted_.load()) {
                Node* tmp = curr_deleted_node;
                curr_deleted_node = curr_deleted_node->next_.load();
                delete tmp;
              }
            }
            counter_.fetch_sub(1);
            return true;
          }
        }
      }
    }
  }

 private:
  Atomic<Node*> head_{nullptr};
  Atomic<Node*> tail_{nullptr};
  Atomic<size_t> counter_{0};
  Atomic<Node*> top_of_deleted_{nullptr};
};

///////////////////////////////////////////////////////////////////////
