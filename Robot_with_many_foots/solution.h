#pragma once

#include <atomic>
#include <mutex>
#include <deque>
#include <condition_variable>
#include <iostream>
#include <thread>


class Semaphore {

 private:

  size_t counter = 0;

  std::mutex mutex;

  std::condition_variable cv;

 public:

  void Enter() {
   std::unique_lock<std::mutex> lock(mutex);
   while (counter <= 0) {
    cv.wait(lock);
   }
   counter--;
  }

  void Leave() {
   std::lock_guard<std::mutex> lock(mutex);
   counter++;
   cv.notify_one();
  }
};


class Robot {

 private:

  std::deque<Semaphore> semaphors;

  size_t num_foots;


 public:

  Robot(const size_t new_num_foots) {
    num_foots = new_num_foots;
    semaphors.resize(num_foots);
    semaphors[0].Leave();
  }

  void Step(const std::size_t foot) {
      semaphors[foot].Enter();
      std::cout << "foot " << foot << std::endl;
      semaphors[(foot + 1) % num_foots].Leave();
  }
};
