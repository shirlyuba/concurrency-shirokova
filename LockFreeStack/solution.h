#pragma once

#include <atomic>
#include <thread>
#include <deque>

///////////////////////////////////////////////////////////////////////

template <typename T>
class LockFreeStack {
  struct Node {
	  T item;
	  std::atomic<Node*> next;

	  Node(T item_) {
	    item = std::move(item_);
	    next.store(nullptr);
	  }
  };

 public:
  explicit LockFreeStack() {
  }

  ~LockFreeStack() {
    Delete(deleted_top_.load());
    Delete(top_.load());
  }

  void Push(T item) {
    auto new_top = new Node(item);
    auto curr_top = top_.load();
    new_top->next.store(curr_top);
    while (!top_.compare_exchange_weak(curr_top, new_top)) {
      new_top->next.store(curr_top);
    }
  }

  bool Pop(T& item) {
    auto curr_top = top_.load();
    while(true) {
      if (!curr_top) {
        return false;
      }
      if (top_.compare_exchange_weak(curr_top, curr_top->next)) {
        item = curr_top->item;
        PushToDeleted(curr_top);
        return true;
      }
    }
  }

 private:

  void PushToDeleted(Node* curr_top) {
    Node* curr_deleted = deleted_top_.load();
    curr_top->next.store(curr_deleted);
    while(!deleted_top_.compare_exchange_weak(curr_deleted, curr_top)) {
      curr_top->next.store(curr_deleted);
    }
  }

  void Delete(Node* item) {
    while (item != nullptr) {
      Node* curr_item = item;
      item = item->next.load();
      delete curr_item;
    }
  }

  std::atomic<Node*> top_{nullptr};
  std::atomic<Node*> deleted_top_{nullptr};
};

///////////////////////////////////////////////////////////////////////

template <typename T>
using ConcurrentStack = LockFreeStack<T>;

///////////////////////////////////////////////////////////////////////
