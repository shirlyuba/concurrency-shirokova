#pragma once

#include <atomic>
#include <mutex>
#include <condition_variable>
#include <iostream>
#include <thread>


class Robot {

 private:

  bool left = true;

  std::mutex mutex;

  std::condition_variable cv_left;

  std::condition_variable cv_right;

public:

    void StepLeft() {
      std::unique_lock<std::mutex> lock(mutex);
      while (!left) {
        cv_left.wait(lock);
      }
      std::cout << "left" << std::endl;
      left = false;
      cv_right.notify_one();
    }

    void StepRight() {
      std::unique_lock<std::mutex> lock(mutex);
      while(left) {
        cv_right.wait(lock);
      }
      std::cout << "right" << std::endl;
      left = true;
      cv_left.notify_one();
    }
};