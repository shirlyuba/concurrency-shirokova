#pragma once

#include <atomic>
#include <mutex>
#include <condition_variable>
#include <iostream>
#include <thread>


class Semaphore {

 private:

  size_t counter = 0;

  std::mutex mutex;

  std::condition_variable cv;

 public:

  void Enter() {
   std::unique_lock<std::mutex> lock(mutex);
   while (counter <= 0) {
    cv.wait(lock);
   }
   counter--;
  }

  void Leave() {
   std::lock_guard<std::mutex> lock(mutex);
   counter++;
   cv.notify_one();
  }
};


class Robot {

 private:

  Semaphore sem_for_left;

  Semaphore sem_for_right;

 public:

  Robot() {
    sem_for_left.Leave();
  }

  void StepLeft() {
    sem_for_left.Enter();
    std::cout << "left" << std::endl;
    sem_for_right.Leave();
  }

  void StepRight() {
    sem_for_right.Enter();
    std::cout << "right" << std::endl;
    sem_for_left.Leave();
  }
};
