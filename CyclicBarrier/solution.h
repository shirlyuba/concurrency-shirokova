#include <atomic>
#include <condition_variable>
#include <thread>


template <typename ConditionVariable = std::condition_variable>
class CyclicBarrier {

 private:

  ConditionVariable cv_for_barrier;

  ConditionVariable cv_for_prebarrier;

  bool is_in_barrier;

  size_t number_of_threads;

  std::mutex mutex;

  size_t threads_in_barrier;

 public:

  CyclicBarrier(size_t new_number) {
    number_of_threads = new_number;
    threads_in_barrier = 0;
    is_in_barrier = false;
  }

  void Pass() {
    std::unique_lock<std::mutex> lock(mutex);
    if (threads_in_barrier > 0) {
      while (is_in_barrier) {
        cv_for_prebarrier.wait(lock);
      }
    } else {
      is_in_barrier = false;
      cv_for_prebarrier.notify_all();
    }
    if (++threads_in_barrier == number_of_threads) {
      is_in_barrier = true;
      cv_for_barrier.notify_all();
    } else {
      while (!is_in_barrier) {
          cv_for_barrier.wait(lock);
      }
    }
    --threads_in_barrier;
  }

};